/*    
 * MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM                                                                                            
 * MM0Z8BBBB8ZMMZ8BBB00880BWMMMM@a80B00ZMMMMMMMZ800ZZ2BMMMMMB                                    MWBWW@M    @BBBBBM      MW0B0B0B0B0W@       B@BWB@W@W   
 * MMX        MM             2MMB       aMMMMM2       aMMMMM@                                    Ma    MB   MB    M@     M           M     0M@       0MM 
 * MMMMB    MMMMMW    MMMM8    MMMB      0MMM0      BMMMMMMMW                                    MB@M  M0   MW@M  MM     M  0M0WM@  MM     MM  M@WBMW  M 
 * MMMMB    MMMMMM    MMMM@   XMMM@       MMM       MMMMMMMMW                                      WM  M8     ZM  MM     M@WBB0WZ @M8      MW  M   MM  M 
 * MMMMW    MMMMMM     2S2   MMMMMW        M        @MMMMMMMB                                      BM  M0     8M  MM         M2  0MBW      MM  M   @M  M 
 * MMMMW    MMMMMM     2     MMMMMW    M       M    @MMMMMMM0                                      WM  M8     8M  MM        aM0BB@  WM     M@  M   @M  M 
 * MMMMM    MMMMMM    MMMM@    MMMM    MM     MM    MMMMMMMMB                                      WM  MB     8M  M@     M@WMM   M0  M     MM  M8  MM  M 
 * MMMMB    MWMMMM    MMMMZ    MMM8    MMM   @MM    0@MMMMMMB                                      WM  MB      M  WM     Ma  M0B0M   M     MM  M@WWM8  M 
 * MMX        MM          22  MMW      MMMB 8MMM      SMMMMMB                                      @M  MM      MZ MM     BMBZ      WMW      MMW     ZWMB 
 * MMWa00B0W8ZMMZ0W@B0BWZ0MMMMMM@Z8WW0ZMMMMBMMMM80WB80MMMMMMB                                       M@@M       MW@MB       BBW@M@@WW          WWW@WBBB   
 * M@MMMMMMMMMMMMMMMMMMMMMMMMMM@MMMMMMMM@WMMMW@MMMMMMMMWWW@MB                                                                                                 
 * 
 * 
 * IBM1130 Loader
 * Original work from Carl Claunch
 * 
 */

//////////////////////////////////////
// Pin definitions
/////////////////////////////////////


#define CES15 22
#define CES14 23
#define CES13 24
#define CES12 25
#define CES11 26
#define CES10 27
#define CES9  28
#define CES8  29
#define CES7  30
#define CES6  31
#define CES5  32
#define CES4  33
#define CES3  34
#define CES2  35
#define CES1  36
#define CES0  37
#define ACTIVATE 4
#define ProgStart 5
#define LoadIAR 6



//////////////////////////////////////////////
// Relay "enabled" state
//////////////////////////////////////////////
#define RELAY_EN   LOW
#define RELAY_DIS  HIGH

#define LOAD_IAR_CMD '@'
#define LOAD_IAR_CMD2 '='
#define TOGGLE_ENABLE_CMD '#'
#define TOGGLE_ENABLE_CMD2 '!'
#define STATUS_CMD '?'
#define TEST_CMD 'T'

int enabled = 0;


//////////////////////////////////////////
// Utils
/////////////////////////////////////////
uint8_t hex2bin(char* word1130_hex, uint16_t* word1130_out){
  int i;
  char bits;
  *word1130_out = 0;
  
  for (i=0; i<4; i++) {
    bits = word1130_hex[i];  // grab each hex character from input
    if(bits >= 48 && bits <= 57){// 0-9 ASCII digits are 48 to 57
          bits -= 48;    
    } else if(bits >= 65 && bits <= 70){// upper case characters A-F are 65-70
          bits -= 65 - 10;// A hex corresponds to 10 dec
    } else if(bits >= 97 && bits <= 102){// lower case characters a-f are 97-102
          bits -= 97 - 10;// A hex corresponds to 10 dec
    } else {
      return 1; // Invalid word
    }
    *word1130_out |= (bits & 0xf) << (4*(3-i));
  }
  return 0;
}

///////////////////////////////////////////
// Bits and buttons
///////////////////////////////////////////
void setCES(uint16_t word1130){
  digitalWrite(CES15, RELAY_DIS ^ bitRead(word1130,15));
  digitalWrite(CES14, RELAY_DIS ^ bitRead(word1130,14));
  digitalWrite(CES13, RELAY_DIS ^ bitRead(word1130,13));
  digitalWrite(CES12, RELAY_DIS ^ bitRead(word1130,12));
  digitalWrite(CES11, RELAY_DIS ^ bitRead(word1130,11));
  digitalWrite(CES10, RELAY_DIS ^ bitRead(word1130,10));
  digitalWrite(CES9, RELAY_DIS ^ bitRead(word1130,9));
  digitalWrite(CES8, RELAY_DIS ^ bitRead(word1130,8));
  digitalWrite(CES7, RELAY_DIS ^ bitRead(word1130,7));
  digitalWrite(CES6, RELAY_DIS ^ bitRead(word1130,6));
  digitalWrite(CES5, RELAY_DIS ^ bitRead(word1130,5));
  digitalWrite(CES4, RELAY_DIS ^ bitRead(word1130,4));
  digitalWrite(CES3, RELAY_DIS ^ bitRead(word1130,3));
  digitalWrite(CES2, RELAY_DIS ^ bitRead(word1130,2));
  digitalWrite(CES1, RELAY_DIS ^ bitRead(word1130,1));
  digitalWrite(CES0, RELAY_DIS ^ bitRead(word1130,0));
  delay(150); // settling delay
 }

 void resetCES(void){
  digitalWrite(CES15, RELAY_DIS);
  digitalWrite(CES14, RELAY_DIS);
  digitalWrite(CES13, RELAY_DIS);
  digitalWrite(CES12, RELAY_DIS);
  digitalWrite(CES11, RELAY_DIS);
  digitalWrite(CES10, RELAY_DIS);
  digitalWrite(CES9, RELAY_DIS);
  digitalWrite(CES8, RELAY_DIS);
  digitalWrite(CES7, RELAY_DIS);
  digitalWrite(CES6, RELAY_DIS);
  digitalWrite(CES5, RELAY_DIS);
  digitalWrite(CES4, RELAY_DIS);
  digitalWrite(CES3, RELAY_DIS);
  digitalWrite(CES2, RELAY_DIS);
  digitalWrite(CES1, RELAY_DIS);
  digitalWrite(CES0, RELAY_DIS); 
 }

 void push_loadIAR(void){
  digitalWrite(LoadIAR, RELAY_EN);
  delay (100);
  digitalWrite(LoadIAR, RELAY_DIS);
  delay (500);
 }

 void push_progStart(void){
  digitalWrite(ProgStart, RELAY_EN);
  delay (100);
  digitalWrite(ProgStart, RELAY_DIS);
  delay (500);
 }
 
 void en_device(void){
  digitalWrite(ACTIVATE, RELAY_EN);
 }

 void dis_device(void){
  digitalWrite(ACTIVATE, RELAY_DIS);
 }

///////////////////////////////////////////
// Initialization
///////////////////////////////////////////
void setup() {
  // put your setup code here, to run once:
  pinMode(CES15, OUTPUT);
  pinMode(CES14, OUTPUT);
  pinMode(CES13, OUTPUT);
  pinMode(CES12, OUTPUT);
  pinMode(CES11, OUTPUT);
  pinMode(CES10, OUTPUT);
  pinMode(CES9, OUTPUT);
  pinMode(CES8, OUTPUT);
  pinMode(CES7, OUTPUT);
  pinMode(CES6, OUTPUT);
  pinMode(CES5, OUTPUT);
  pinMode(CES4, OUTPUT);
  pinMode(CES3, OUTPUT);
  pinMode(CES2, OUTPUT);
  pinMode(CES1, OUTPUT);
  pinMode(CES0, OUTPUT);
  pinMode(ACTIVATE, OUTPUT);
  pinMode(ProgStart, OUTPUT);
  pinMode(LoadIAR, OUTPUT);
  digitalWrite(CES15, RELAY_DIS);
  digitalWrite(CES14, RELAY_DIS);
  digitalWrite(CES13, RELAY_DIS);
  digitalWrite(CES12, RELAY_DIS);
  digitalWrite(CES11, RELAY_DIS);
  digitalWrite(CES10, RELAY_DIS);
  digitalWrite(CES9, RELAY_DIS);
  digitalWrite(CES8, RELAY_DIS);
  digitalWrite(CES7, RELAY_DIS);
  digitalWrite(CES6, RELAY_DIS);
  digitalWrite(CES5, RELAY_DIS);
  digitalWrite(CES4, RELAY_DIS);
  digitalWrite(CES3, RELAY_DIS);
  digitalWrite(CES2, RELAY_DIS);
  digitalWrite(CES1, RELAY_DIS);
  digitalWrite(CES0, RELAY_DIS);
  digitalWrite(ACTIVATE, RELAY_DIS);
  digitalWrite(ProgStart, RELAY_DIS);
  digitalWrite(LoadIAR, RELAY_DIS);
  digitalWrite(ACTIVATE,  RELAY_DIS);

  // Start up our link with the terminal user
  Serial.begin(9600);
  Serial.println("IBM 1130 Loader is waiting for activation");
}

///////////////////////////////////////////
// Main loop
///////////////////////////////////////////
void loop() {
  char incomingchar = 0;
  char newword[5] = {0,0,0,0,0};
  int word_idx = 0;
  int loadIAR = 0;
  uint16_t word_1130;
  
  // New command
  Serial.print(">");

  while(Serial.available() == 0);// Wait char available
  incomingchar = Serial.read();
  Serial.print(incomingchar);// Echo

  // Blank line
  if ((incomingchar == '\n') || (incomingchar == '\r')) {
    Serial.println("");
    return;
  }

  // Toggle en CMD
  if(incomingchar == TOGGLE_ENABLE_CMD || incomingchar == TOGGLE_ENABLE_CMD2){
    Serial.println("");
    if(enabled == 0){
      en_device();
      Serial.println("Activated - device has control of Program Start button");
    } else {
      dis_device();
      Serial.println("Deactivated - Program Start button can be used normally");
    }
    enabled ^= 1;// Toggle activated status
    return; // Goto next cmd
  }

  // Get status CMD
  if(incomingchar == STATUS_CMD){
    Serial.println("");
    if(enabled = 0){
      Serial.println("Activated - device has control of Program Start button");
    } else {
      Serial.println("Deactivated - Program Start button can be used normally");
    }
    return; // Goto next cmd
  }

  // Loard IAR CMD
  if(incomingchar == LOAD_IAR_CMD || incomingchar == LOAD_IAR_CMD2){
    loadIAR = 1;
  } else {
    newword[word_idx] = incomingchar;
    word_idx++;
  }

  // Get Word
  for(; word_idx < 4; word_idx++){
    while(Serial.available() == 0);// Wait char available
    incomingchar = Serial.read();
    // Cancel load
    if ((incomingchar == '\n') || (incomingchar == '\r')) {
          Serial.println("");
          Serial.println("Dropping partial word");
          return;
    }
    Serial.print(incomingchar);// Echo
    newword[word_idx] = incomingchar;
  }
  
  // Convert word
  if(hex2bin(newword, &word_1130)){
    Serial.println("");
    Serial.println("Not a valid hex word, dropped");
    return;
  }

  // Load word
  setCES(word_1130);

  Serial.println("");
  if(loadIAR == 1){
    Serial.print("Setting IAR to:");
  }else {
    Serial.print("Loading:"); 
  }
  Serial.println(word_1130,BIN);
   
  if(loadIAR == 1){
    push_loadIAR();
  }else {
    push_progStart();
  }
  resetCES();
}
