import serial
import sys


def wait_end_of_cmd(ser):
    end = True
    while end is True:
        loader_line = ser.readline()
        loader_line = bytearray(loader_line).decode()
        print(str(loader_line), end='')
        if loader_line:
            if loader_line[0] == '>':
                return


def main():
    if len(sys.argv) < 3:
        print("Not enough arguments")
        exit(0)

    com_path = sys.argv[1]
    file_path = sys.argv[2]

    with open(file_path) as f:
        with serial.Serial(com_path, 9600, timeout=0) as ser:
            # Check connexion
            ser.write("\n".encode())
            wait_end_of_cmd(ser)
            # Enable device
            ser.write('!'.encode())
            wait_end_of_cmd(ser)

            # Load file
            loading = True
            while loading:
                file_line = f.readline()
                if not file_line:
                    break
                file_line = file_line.replace(' ', '').replace('\n', '').replace('\r', '')
                ser.write(file_line.encode())
                wait_end_of_cmd(ser)
            # Disable device
            ser.write('!'.encode())
            wait_end_of_cmd(ser)


if __name__ == "__main__":
    main()
